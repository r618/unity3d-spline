# README #

This is some sample code to go along with [Unite 2015 - A coder's guide to spline-based procedural geometry](https://www.youtube.com/watch?v=o9RK6O2kOKo), since that author did not include any sample projects.

Note that this is an extremely basic implementation and does not include the spline designer and likely has a lot of memory leaks.
The code for the monobehaviour is poorly commented and even more poorly written.

It is currently a proof of concept only.