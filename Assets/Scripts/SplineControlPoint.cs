﻿using UnityEngine;
using System.Collections;
using System.Linq;

/// <summary>
/// Bind a control widget to a road's bezier curve
/// </summary>
[ExecuteInEditMode]
public class SplineControlPoint : MonoBehaviour
{
    public int curveIndex;
    public int ptIndex;
    public Color color;

    SplineRoad splineRoad;
    SplineRoad SplineRoad
    {
        get
        {
            if (splineRoad == null)
            {
                splineRoad = transform.parent.gameObject.GetComponent<SplineRoad>();
            }
            return splineRoad;
        }
    }

    void Start()
    {
        transform.Translate(new Vector3(0, 0, 0));
    }

    void Update()
    {
        if (transform.hasChanged)
        {
            BezierCurve curve = SplineRoad.Curves.ElementAtOrDefault(curveIndex);
            if (curve != null)
            {
                curve.SetPoint(ptIndex, transform.localPosition);
            }

            SplineRoad.CreateMesh();
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = this.color;
        Gizmos.DrawWireSphere(transform.position, 0.1f);
    }
}

