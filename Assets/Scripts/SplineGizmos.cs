﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[ExecuteInEditMode]
[RequireComponent(typeof(SplineRoad))]
public class SplineGizmos : MonoBehaviour
{
    SplineRoad Road
    {
        get
        {
            return GetComponent<SplineRoad>();
        }
    }

    // Use this for initialization
    void Start()
    {
    }

    void Update()
    {
        if (Application.isEditor)
        {
            //road = GetComponent<SplineRoad>();

            //Dictionary<BezierCurve, bool> curves = road.Curves
            //    .ToDictionary(c => c, c => true);

            //SplineControlPoint[] controlPoints = gameObject
            //    .GetComponentsInChildren<SplineControlPoint>();

            //// Add control points for any curves that don't exist, yet
            //BezierCurve[] curveList = Road.Curves.ToArray();
            //BezierCurve firstCurve = curveList.FirstOrDefault();
            //var missingCurveControlPoints = controlPoints.Values
            //    .Where(cip => cip != null)
            //    .Select(cip => cip.curveIndex);

            //foreach (BezierCurve c in Road.Curves)
            //{
            //    if (c == firstCurve)
            //    {
            //        AddControlPoint(c, 0, Color.cyan);
            //        AddControlPoint(c, 1, Color.green);
            //    }
            //    AddControlPoint(c, 2, Color.green);
            //    AddControlPoint(c, 3, Color.cyan);

            //    UnityEditorInternal.InternalEditorUtility.RepaintAllViews();
            //}

            ////// Remove any control points that are no longer curves
            ////foreach (var kv in controlPoints)
            ////{
            ////    if (kv.Value == null || kv.Value.curveIndex == null || curves.ContainsKey(kv.Value.curveIndex) == false)
            ////    {
            ////        DestroyImmediate(kv.Key.gameObject);
            ////    }
            ////}
        }
    }

    void OnDrawGizmos()
    {
        if (Road.drawSpline)
        {
            foreach (BezierCurve c in Road.Curves)
            {
                Gizmos.color = Color.gray;
                Gizmos.DrawLine(c.GetPoint((int)0), c.GetPoint((int)1));
                Gizmos.DrawLine(c.GetPoint((int)2), c.GetPoint((int)3));

                for (int i = 0; i < c.SampledPoints.Length - 1; i++)
                {
                    Gizmos.color = Color.white;
                    Gizmos.DrawLine(c.SampledPoints[i], c.SampledPoints[i + 1]);
                }
            }
        }

    }
}
